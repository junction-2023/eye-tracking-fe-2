const assets = {
    videos: {
        driving_p1:
            'https://pixieray-video-for-junction.s3.eu-north-1.amazonaws.com/Participant_1-Driving-720p.mp4',
    },
    chart: {
        minimized_driving_z_angle:
            'https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/junction23/minimized_driving_z_angle.jpeg',
        minimized_indoor_z_angle:
            'https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/junction23/minimized_indoor_z_angle.jpeg',
        minimized_walking_z_angle:
            'https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/junction23/minimized_walking_z_angle.jpeg',
    },
}

export default assets
