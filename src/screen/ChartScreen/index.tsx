import { BsBell } from 'react-icons/bs'
import { IoIosArrowDown } from 'react-icons/io'
import assets from '~root/assets'
import ChartBlock from './ChartBlock'

interface IChartBlock {
    title: string
    subtitle: string
    chartUrl: string
    description: string
    result: string
}

const CHART_LIST: IChartBlock[] = [
    {
        title: 'First 100 z angle of 3 participants in driving scenario',
        subtitle: '',
        chartUrl: assets.chart.minimized_driving_z_angle,
        description:
            'The three lines describe the degree of the head between the upward direction and the forward direction of three participants while driving in the first 100 records. There is also a threshold in the chart which is used to determine whether the participant is tired or not. The participants whose median values lie below this line are consider not tired, while their counterparts are tired and needs to rest.',
        result: 'It is determinable whether a user is tired or not based on their head position and movement, and therefore have appropriate actions',
    },
    {
        title: 'First 100 z angle of 3 participants in indoor (reading) scenario',
        subtitle: '',
        chartUrl: assets.chart.minimized_indoor_z_angle,
        description:
            'The three lines describe the degree of the head between the upward direction and the forward direction of three participants while staying indoor and reading a piece of paper in the first 100 records. There is also a threshold in the chart which is used to determine whether the participant is tired or not. The participants whose median values lie below this line are consider not tired, while their counterparts are tired and needs to rest.',
        result: 'It is determinable whether a user is tired or not based on their head position and movement, and therefore have appropriate actions',
    },
    {
        title: 'First 100 z angle of 3 participants in walking scenario',
        subtitle: '',
        chartUrl: assets.chart.minimized_walking_z_angle,
        description:
            'The three lines describe the degree of the head between the upward direction and the forward direction of three participants while walking in the first 100 records. There is also a threshold in the chart which is used to determine whether the participant is tired or not. The participants whose median values lie below this line are consider not tired, while their counterparts are tired and needs to rest.',
        result: 'It is determinable whether a user is tired or not based on their head position and movement, and therefore have appropriate actions',
    },
]

export default function ChartScreen() {
    return (
        <div className='w-full h-screen flex flex-col p-8 overflow-scroll'>
            <div className='flex items-center justify-between'>
                <h3 className='text-4xl text-default'>Chart & Exploration</h3>
                <div className='flex items-center'>
                    <div className='bg-white py-2 px-3 shadow-md rounded-md hover:shadow-lg transition-shadow cursor-pointer'>
                        <BsBell className='text-xl' />
                    </div>
                    <div className='flex justify-center  bg-white py-2 px-3 shadow-md rounded-md ml-4 relative hover:shadow-lg transition-shadow cursor-pointer'>
                        <img
                            className='w-6 h-6 rounded-full '
                            src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAAAAABWESUoAAABWElEQVR4AaWSIWjDYBCFv/95qmYn66aqYqsK9TNR8QTiITBfOzk/WVlVb6pqKyvqCVQVcrtwLD/N4vaZ3L1cjsflwaptmpIpSWybpl3jXM2sZYKoXO4WkFh01tuWxIgSvPVmj1X0G7P+DhkJbvawCoXQ+oormQQnlz6RCL59/JBbMQhHyJxd2CEIGrNhZSIzGC1/lbW5wRfEiFgO2hIlQJ03Gya2K19xi+7i5QfiicSXyyecg/vZQ2LK0cwq2Pjgmb8IrtYgUT465ik241PMoPyUmCGhsUz8l8Qc211E4aNEzNHZIX7t/XV25zmiN9z8Eque2fuLQ86CxDNt/vDs5e7ZZ6Jw8b4YzZi9IyUyj36MgFj1zhIRJEUE2hz6OsKRCBQRIJPDEewmERAcXfqKRrx7003Pe3OxjunXu9fF9DJLt90XxKxZPV2QKM3i5qu6qrbMsK6quuAHDgrRaprttGsAAAAASUVORK5CYII='
                            alt='user avatar'
                        />
                        <h4 className='font-medium mx-3 text-default'>Pixieray</h4>
                        <div className='flex justify-center items-center'>
                            <IoIosArrowDown className=''></IoIosArrowDown>
                        </div>
                    </div>
                </div>
            </div>

            <div className='w-full h-fit relative'>
                {CHART_LIST.map((chart) => (
                    <ChartBlock
                        key={chart.title}
                        title={chart.title}
                        subtitle={chart.subtitle}
                        chartUrl={chart.chartUrl}
                        description={chart.description}
                        result={chart.result}
                    />
                ))}
            </div>
        </div>
    )
}
