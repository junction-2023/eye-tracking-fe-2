interface IChartBlockProps {
    title: string
    subtitle: string
    chartUrl: string
    description: string
    result: string
}

export default function ChartBlock(props: IChartBlockProps) {
    const { title, subtitle, chartUrl, description, result } = props

    return (
        <div className='grid grid-cols-3 gap-6 mt-12 '>
            <div className='col-span-2 flex justify-center bg-white h-fit rounded-2xl shadow-lg  p-8'>
                <div className=' w-[90%] items-center justify-between'>
                    <div className='flex flex-col '>
                        <h3 className='text-3xl text-default'>{title}</h3>
                        <p className='text-base text-default/50 mt-1'>{subtitle}</p>
                        <div className='w-full mt-4'>
                            <img className='w-full' src={chartUrl} alt={title} />
                        </div>
                    </div>
                </div>
            </div>
            <div className='col-span-1 '>
                <div className='flex flex-col bg-white h-fit rounded-2xl shadow-lg p-8'>
                    <h4 className='py-3 text-xl border-b-[1px] border-solid border-black/60'>
                        Annotation
                    </h4>
                    <div className='flex flex-col mt-4'>
                        <div className='flex items-center mb-2'>
                            <div className='bg-orange-400 w-6 h-4 mr-4 '></div>
                            <p className='text-base'>Participant 1</p>
                        </div>
                        <div className='flex items-center mb-2'>
                            <div className='bg-blue-400 w-6 h-4 mr-4'></div>
                            <p className='text-base'>Participant 2</p>
                        </div>
                        <div className='flex items-center '>
                            <div className='bg-green-400 w-6 h-4 mr-4'></div>
                            <p className='text-base'>Participant 3</p>
                        </div>
                    </div>
                </div>
                <div className='w-full bg-white h-fit rounded-2xl shadow-lg px-8 py-6 mt-6'>
                    <p>{description}</p>
                </div>
                <div className='w-full bg-white h-fit rounded-2xl shadow-lg px-8 py-6 mt-6'>
                    <h4 className='py-1 w-2/3 text-xl border-b-[1px] border-solid border-black/60'>
                        Result
                    </h4>
                    <p className='mt-2'>{result}</p>
                </div>
            </div>
        </div>
    )
}
