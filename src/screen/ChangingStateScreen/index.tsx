import React from 'react'
import { BsBell } from 'react-icons/bs'
import { IoIosArrowDown } from 'react-icons/io'
import { FaBalanceScaleLeft } from 'react-icons/fa'
import RecordItem from './RecordItem'
import CustomVideo from './CustomVideo'
import classNames from 'classnames'
import assets from '~root/assets'
import * as z_data from '../../../data/minimized_driving_p1_imu_z_angle.json'
import * as x_data from '../../../data/minimized_driving_p1_imu_x_angle.json'

const zObject: any = JSON.parse(JSON.stringify(z_data))
const xObject: any = JSON.parse(JSON.stringify(x_data))

const Z_DATA: number[] = []
const X_DATA: number[] = []

for (const value in zObject.default) {
    Z_DATA.push(zObject.default[value])
}

for (const value in xObject.default) {
    X_DATA.push(xObject.default[value])
}

export type TScenario = 'Walking' | 'Indoor' | 'Driving'
type TParticipant = 1 | 2 | 3

// const generateVideo = (scenario: TScenario, participant: TParticipant) => {
//     switch (scenario) {
//         case 'Walking':
//             if (participant === 1) return assets.videos.walking_p1
//             else if (participant === 2) return assets.videos.walking_p2
//             else return assets.videos.walking_p3
//         case 'Indoor':
//             if (participant === 1) return assets.videos.indoor_p1
//             else if (participant === 2) return assets.videos.indoor_p2
//             else return assets.videos.indoor_p3
//         case 'Driving':
//             if (participant === 1) return assets.videos.driving_p1
//             else if (participant === 2) return assets.videos.driving_p2
//             else return assets.videos.driving_p3
//     }
// }

export const SCENARIOS: { title: TScenario }[] = [
    {
        title: 'Walking',
    },
    {
        title: 'Indoor',
    },
    {
        title: 'Driving',
    },
]

const PARTICIPANTS: TParticipant[] = [1, 2, 3]

export default function ChangingStateScreen() {
    const [scenario, setScenario] = React.useState<TScenario>('Driving')
    const [participant, setParticipant] = React.useState<TParticipant>(1)
    const [isRun, setIsRun] = React.useState<boolean>(false)

    const handleRunSimulation = () => {
        setIsRun(true)
        console.log('Play')
    }

    return (
        <div className='w-full flex flex-col p-8'>
            <div className='flex items-center justify-between'>
                <h3 className='text-4xl text-default'>Changing State</h3>
                <div className='flex items-center'>
                    <div className='bg-white py-2 px-3 shadow-md rounded-md hover:shadow-lg transition-shadow cursor-pointer'>
                        <BsBell className='text-xl' />
                    </div>
                    <div className='flex justify-center  bg-white py-2 px-3 shadow-md rounded-md ml-4 relative hover:shadow-lg transition-shadow cursor-pointer'>
                        <img
                            className='w-6 h-6 rounded-full '
                            src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAAAAABWESUoAAABWElEQVR4AaWSIWjDYBCFv/95qmYn66aqYqsK9TNR8QTiITBfOzk/WVlVb6pqKyvqCVQVcrtwLD/N4vaZ3L1cjsflwaptmpIpSWybpl3jXM2sZYKoXO4WkFh01tuWxIgSvPVmj1X0G7P+DhkJbvawCoXQ+oormQQnlz6RCL59/JBbMQhHyJxd2CEIGrNhZSIzGC1/lbW5wRfEiFgO2hIlQJ03Gya2K19xi+7i5QfiicSXyyecg/vZQ2LK0cwq2Pjgmb8IrtYgUT465ik241PMoPyUmCGhsUz8l8Qc211E4aNEzNHZIX7t/XV25zmiN9z8Eque2fuLQ86CxDNt/vDs5e7ZZ6Jw8b4YzZi9IyUyj36MgFj1zhIRJEUE2hz6OsKRCBQRIJPDEewmERAcXfqKRrx7003Pe3OxjunXu9fF9DJLt90XxKxZPV2QKM3i5qu6qrbMsK6quuAHDgrRaprttGsAAAAASUVORK5CYII='
                            alt='user avatar'
                        />
                        <h4 className='font-medium mx-3 text-default'>Pixieray</h4>
                        <div className='flex justify-center items-center'>
                            <IoIosArrowDown className=''></IoIosArrowDown>
                        </div>
                    </div>
                </div>
            </div>
            <div className='grid grid-cols-3 gap-6 mt-10'>
                <div className='col-span-2 bg-white h-fit rounded-2xl shadow-lg  p-8'>
                    <div className='flex w-full items-center justify-between'>
                        <div className='flex flex-col'>
                            <h3 className='text-3xl text-default'>Recorded video</h3>
                            <p className='text-base text-default/50 mt-1'>
                                This video is being analyzed{' '}
                            </p>
                        </div>
                        <div className=''></div>
                    </div>
                    <div className='flex items-center mt-6'>
                        <h3 className=' text-lg font-medium mr-4'>Participant: </h3>
                        <div className='flex items-center'>
                            {PARTICIPANTS.map((no) => (
                                <button
                                    key={no}
                                    onClick={() => setParticipant(no)}
                                    className={classNames('p-2  rounded-md mr-4 cursor-pointer', {
                                        'bg-slate-100': participant !== no,
                                        'bg-teal-300': participant === no,
                                    })}
                                >
                                    {no.toString()}
                                </button>
                            ))}
                        </div>
                    </div>
                    <div className='flex items-center mt-6'>
                        <h3 className=' text-lg font-medium mr-4'>Scenario: </h3>
                        <div className='flex items-center'>
                            {SCENARIOS.map((name) => (
                                <button
                                    key={name.title}
                                    onClick={() => setScenario(name.title)}
                                    className={classNames('p-2  rounded-md mr-4 cursor-pointer', {
                                        'bg-slate-100': scenario !== name.title,
                                        'bg-teal-300': scenario === name.title,
                                    })}
                                >
                                    {name.title}
                                </button>
                            ))}
                        </div>
                    </div>
                    <div className='w-full h-96 mt-10'>
                        <CustomVideo
                            className='h-full'
                            url={assets.videos.driving_p1}
                            func={handleRunSimulation}
                            light={
                                'https://xpvurtyfnuyuvkehgqoo.supabase.co/storage/v1/object/public/junction23/Screenshot%202023-11-12%20at%2003.20.08.png'
                            }
                        />
                    </div>
                </div>
                <div className='col-span-1 bg-white h-fit rounded-2xl shadow-lg p-8'>
                    <RecordItem title='Balance' isRun={isRun} data={X_DATA}>
                        <FaBalanceScaleLeft className='mr-3 text-2xl mx-1' />
                    </RecordItem>
                    <RecordItem title='Posture' isRun={isRun} data={Z_DATA}>
                        <img
                            className='h-8 mr-1'
                            src='https://cdn-icons-png.flaticon.com/512/6947/6947970.png'
                            alt='posture'
                        />
                    </RecordItem>
                    <p className='mt-4 text-default'>
                        This simulator will only show the balance and posture of the participant 1
                        when driving. Because of lack of storage for videos, we only simulate one
                        scenarios and hope that it will show you how the glass works. Please choose
                        Driving scenario and Participant 1 to get the best experience.
                    </p>
                </div>
            </div>
        </div>
    )
}
