import React from 'react'

interface IRecordItemProps {
    title: string
    children: React.ReactNode
    isRun: boolean
    data: number[]
}

export default function RecordItem(props: IRecordItemProps) {
    const { children, title, data, isRun } = props

    const [index, setIndex] = React.useState<number>(0)

    React.useEffect(() => {
        let handle = 0
        if (isRun) {
            handle = window.setInterval(() => {
                setIndex((prev) => prev + 6)
            }, 100)
        }

        return () => {
            clearInterval(handle)
        }
    }, [isRun])

    return (
        <div className='flex flex-col mb-7'>
            <div className='flex items-center'>
                {children}
                <h3 className=' text-default font-medium text-2xl'>{title}</h3>
            </div>
            <p className='font-bold mt-2 text-4xl text-teal-300'>{data[index]?.toFixed(2)}°</p>
        </div>
    )
}
