import { useState, useEffect } from 'react'
import ReactPlayer from 'react-player'

interface ICustomVideoProps {
    className?: string
    url: string
    light?: string
    width?: string
    height?: string
    func?: () => void
}

export default function CustomVideo(props: ICustomVideoProps) {
    const { url, className, light, func } = props
    const [hasWindow, setHasWindow] = useState<boolean>(false)

    const [index, setIndex] = useState(0)

    useEffect(() => {
        window.setInterval(() => {
            setIndex((prev) => prev + 6)
        }, 100)
    }, [])

    useEffect(() => {
        if (typeof window !== undefined) {
            setHasWindow(true)
        }
    }, [])

    return (
        <div className={className}>
            {hasWindow && (
                <ReactPlayer
                    width='100%'
                    height='100%'
                    light={light}
                    url={url}
                    controls
                    onPlay={func}
                    playing={index <= 4000}
                ></ReactPlayer>
            )}
        </div>
    )
}
