import { useLocation, useNavigate } from 'react-router-dom'
import { BiHomeSmile } from 'react-icons/bi'
import classNames from 'classnames'
import { BiFile } from 'react-icons/bi'

export default function Sidebar() {
    const location = useLocation()
    const navigate = useNavigate()

    const handleNavigate = (path: string) => {
        navigate(path, { replace: false })
    }

    const routes = [
        {
            label: 'Home',
            icon: <BiHomeSmile className='mr-4 text-xl' />,
            href: '/',
        },
        {
            label: 'Chart',
            icon: <BiFile className='mr-4 text-xl' />,
            href: '/chart',
        },
    ]

    return (
        <div className='w-1/5 h-screen bg-slate-50'>
            <div className='p-5 pt-10'>
                <div className='flex p-2'>
                    <img
                        src='https://marketplace.canva.com/vhV-E/MAFHSMvhV-E/1/tl/canva-MAFHSMvhV-E.png'
                        alt=''
                        className='w-8 h-8 '
                    />
                    <p className='font-semibold text-default text-lg pl-2 pt-2'>Myfocus.io</p>
                </div>
                <div className='list-none pt-5'>
                    {routes.map((route) => (
                        <div
                            key={route.label}
                            onClick={() => handleNavigate(route.href)}
                            className={classNames(
                                'flex items-center font-medium text-default text-lg my-10 py-2.5 pl-3 rounded-md hover:bg-teal-400 hover:text-white cursor-pointer transition-all',
                                {
                                    'bg-teal-400': route.href === location.pathname,
                                },
                            )}
                        >
                            {route.icon}
                            {route.label}
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}
